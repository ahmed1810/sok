package com.example.scctor.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Posts implements Parcelable {

    private String emr_Id;
    private String longtitude;
    private String latitude;
    private String feed_Date;
    private String user_Name;
    private String status;
    private String active_People;
    private String user_Id;
    private String child_id;

    protected Posts(Parcel in) {
        emr_Id = in.readString();
        longtitude = in.readString();
        latitude = in.readString();
        feed_Date = in.readString();
        user_Name = in.readString();
        status = in.readString();
        active_People = in.readString();
        user_Id = in.readString();
        child_id = in.readString();
    }

    public static final Creator<Posts> CREATOR = new Creator<Posts>() {
        @Override
        public Posts createFromParcel(Parcel in) {
            return new Posts(in);
        }

        @Override
        public Posts[] newArray(int size) {
            return new Posts[size];
        }
    };

    public String getEmr_Id() {
        return emr_Id;
    }

    public void setEmr_Id(String emr_Id) {
        this.emr_Id = emr_Id;
    }

    public String getLongtitude() {
        return longtitude;
    }

    public void setLongtitude(String longtitude) {
        this.longtitude = longtitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getFeed_Date() {
        return feed_Date;
    }

    public void setFeed_Date(String feed_Date) {
        this.feed_Date = feed_Date;
    }

    public String getUser_Name() {
        return user_Name;
    }

    public void setUser_Name(String user_Name) {
        this.user_Name = user_Name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getActive_People() {
        return active_People;
    }

    public void setActive_People(String active_People) {
        this.active_People = active_People;
    }

    public String getUser_Id() {
        return user_Id;
    }

    public void setUser_Id(String user_Id) {
        this.user_Id = user_Id;
    }

    public String getChild_id() {
        return child_id;
    }

    public void setChild_id(String child_id) {
        this.child_id = child_id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(emr_Id);
        dest.writeString(longtitude);
        dest.writeString(latitude);
        dest.writeString(feed_Date);
        dest.writeString(user_Name);
        dest.writeString(status);
        dest.writeString(active_People);
        dest.writeString(user_Id);
        dest.writeString(child_id);
    }
}
