package com.example.scctor.model.allchildern;

import java.util.List;

public class ChildernResponse {

    private String error;
    private List<childern> chlidren;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public List<childern> getChilderns() {
        return chlidren;
    }

    public void setChilderns(List<childern> childerns) {
        this.chlidren = childerns;
    }
}
