package com.example.scctor.model.active_users;

public class Active_Users {

    private String active_Id;

    private String user_Id;

    private String user_longtitude;

    private String emr_Id;

    private String user_latitude;

    public String getActive_Id ()
    {
        return active_Id;
    }

    public void setActive_Id (String active_Id)
    {
        this.active_Id = active_Id;
    }

    public String getUser_Id ()
    {
        return user_Id;
    }

    public void setUser_Id (String user_Id)
    {
        this.user_Id = user_Id;
    }

    public String getUser_longtitude ()
    {
        return user_longtitude;
    }

    public void setUser_longtitude (String user_longtitude)
    {
        this.user_longtitude = user_longtitude;
    }

    public String getEmr_Id ()
    {
        return emr_Id;
    }

    public void setEmr_Id (String emr_Id)
    {
        this.emr_Id = emr_Id;
    }

    public String getUser_latitude ()
    {
        return user_latitude;
    }

    public void setUser_latitude (String user_latitude)
    {
        this.user_latitude = user_latitude;
    }
}
