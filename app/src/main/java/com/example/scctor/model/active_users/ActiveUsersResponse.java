package com.example.scctor.model.active_users;

import java.util.List;

public class
ActiveUsersResponse {
    private List<Active_Users> active_Users;

    private String error;

    public List<Active_Users> getActive_Users() {
        return active_Users;
    }

    public String getError() {
        return error;
    }
}
