package com.example.scctor.model.allchildern;

public class childern {
    private String ch_Id;
    private String par_Id;
    private String child_Name;
    private String birthday;
    private String gender;
    private String child_img_url;

    public String getChild_img_url() {
        return child_img_url;
    }

    public void setChild_img_url(String child_img_url) {
        this.child_img_url = child_img_url;
    }

    public String getCh_Id() {
        return ch_Id;
    }

    public void setCh_Id(String ch_Id) {
        this.ch_Id = ch_Id;
    }

    public String getPar_Id() {
        return par_Id;
    }

    public void setPar_Id(String par_Id) {
        this.par_Id = par_Id;
    }

    public String getChild_Name() {
        return child_Name;
    }

    public void setChild_Name(String child_Name) {
        this.child_Name = child_Name;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
