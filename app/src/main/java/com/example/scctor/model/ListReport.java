package com.example.scctor.model;

import java.util.List;

public class ListReport {

private String error;
private List<Posts> posts;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public List getPosts() {
        return posts;
    }

    public void setPosts(List posts) {
        this.posts = posts;
    }
}
