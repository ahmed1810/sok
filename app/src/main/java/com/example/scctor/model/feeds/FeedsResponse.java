package com.example.scctor.model.feeds;

import java.util.List;

public class FeedsResponse {

    private List<Feeds> feeds;
    private String error;

    public List<Feeds> getFeeds() {
        return feeds;
    }

    public String getError() {
        return error;
    }
}
