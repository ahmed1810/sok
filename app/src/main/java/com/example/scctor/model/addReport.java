package com.example.scctor.model;

public class addReport {


    private String longtitude;
    private String latitude;
    private String feed_Date;
    private String user_Name;
    private String status;
    private String special_info;
    private String user_Id;
    private String child_id;
    private String emr_Date;

    public addReport(String longtitude, String latitude, String feed_Date, String user_Name, String status, String special_info, String user_Id, String child_id, String emr_Date) {
        this.longtitude = longtitude;
        this.latitude = latitude;
        this.feed_Date = feed_Date;
        this.user_Name = user_Name;
        this.status = status;
        this.special_info = special_info;
        this.user_Id = user_Id;
        this.child_id = child_id;
        this.emr_Date = emr_Date;
    }
}
