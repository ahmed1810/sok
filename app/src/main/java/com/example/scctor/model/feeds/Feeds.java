package com.example.scctor.model.feeds;


public class Feeds
{
    private String user_Name;

    private String feed_img;

    private String user_Image;

    private String content;

    private String feed_Id;

    private String feed_Date;

    public String getUser_Name ()
    {
        return user_Name;
    }

    public void setUser_Name (String user_Name)
    {
        this.user_Name = user_Name;
    }

    public String getFeed_img ()
    {
        return feed_img;
    }

    public void setFeed_img (String feed_img)
    {
        this.feed_img = feed_img;
    }

    public String getUser_Image ()
    {
        return user_Image;
    }

    public void setUser_Image (String user_Image)
    {
        this.user_Image = user_Image;
    }

    public String getContent ()
    {
        return content;
    }

    public void setContent (String content)
    {
        this.content = content;
    }

    public String getFeed_Id ()
    {
        return feed_Id;
    }

    public void setFeed_Id (String feed_Id)
    {
        this.feed_Id = feed_Id;
    }

    public String getFeed_Date ()
    {
        return feed_Date;
    }

    public void setFeed_Date (String feed_Date)
    {
        this.feed_Date = feed_Date;
    }
}
