package com.example.scctor.model.emergencyInfo;

public class Emergency_Info {
    private String birthday;

    private String child_img_url;

    private String emr_Date;

    private String child_id;

    private String gender;

    private String user_Id;

    private String special_Info;

    private String active_People;

    private String child_Name;

    private String emr_Id;

    private String status;

    public String getBirthday ()
    {
        return birthday;
    }

    public void setBirthday (String birthday)
    {
        this.birthday = birthday;
    }

    public String getChild_img_url ()
    {
        return child_img_url;
    }

    public void setChild_img_url (String child_img_url)
    {
        this.child_img_url = child_img_url;
    }

    public String getEmr_Date ()
    {
        return emr_Date;
    }

    public void setEmr_Date (String emr_Date)
    {
        this.emr_Date = emr_Date;
    }

    public String getChild_id ()
    {
        return child_id;
    }

    public void setChild_id (String child_id)
    {
        this.child_id = child_id;
    }

    public String getGender ()
    {
        return gender;
    }

    public void setGender (String gender)
    {
        this.gender = gender;
    }

    public String getUser_Id ()
    {
        return user_Id;
    }

    public void setUser_Id (String user_Id)
    {
        this.user_Id = user_Id;
    }

    public String getSpecial_Info ()
    {
        return special_Info;
    }

    public void setSpecial_Info (String special_Info)
    {
        this.special_Info = special_Info;
    }

    public String getActive_People ()
    {
        return active_People;
    }

    public void setActive_People (String active_People)
    {
        this.active_People = active_People;
    }

    public String getChild_Name ()
    {
        return child_Name;
    }

    public void setChild_Name (String child_Name)
    {
        this.child_Name = child_Name;
    }

    public String getEmr_Id ()
    {
        return emr_Id;
    }

    public void setEmr_Id (String emr_Id)
    {
        this.emr_Id = emr_Id;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

}
