package com.example.scctor.model.emergencyInfo;

import java.util.List;

public class EmergencyResponse {

    private List<Emergency_Info> emergency_Info;

    private String error;

    public List<Emergency_Info> getEmergency_Info() {
        return emergency_Info;
    }

    public String getError() {
        return error;
    }
}
