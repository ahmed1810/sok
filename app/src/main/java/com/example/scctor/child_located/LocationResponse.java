package com.example.scctor.child_located;

import java.util.List;

public class LocationResponse {

    private String error;

    private String message;

    private List<Parent_Info> parent_Info;

    public String getError() {
        return error;
    }

    public String getMessage() {
        return message;
    }

    public List<Parent_Info> getParent_Info() {
        return parent_Info;
    }
}
