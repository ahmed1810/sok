package com.example.scctor.network;


import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceGenerator {
    public static final String BASE_URL = "http://saveourkids.000webhostapp.com/saveOurKids/public/";

    public static Retrofit retrofit = null;

    public static Retrofit getApiClient() {
        retrofit = new Retrofit.Builder().baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).build();
        return retrofit;
    }

}
