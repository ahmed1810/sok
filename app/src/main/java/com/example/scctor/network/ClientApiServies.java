package com.example.scctor.network;


import com.example.scctor.child_located.LocationResponse;
import com.example.scctor.model.ListReport;
import com.example.scctor.model.LoginResponse;
import com.example.scctor.model.RegsiterResponse;
import com.example.scctor.model.active_users.ActiveUsersResponse;
import com.example.scctor.model.allchildern.ChildernResponse;
import com.example.scctor.model.emergencyInfo.EmergencyResponse;
import com.example.scctor.model.feeds.FeedsResponse;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface ClientApiServies {

    @FormUrlEncoded
    @Headers({
            "Accept: application/json",
    })
    @POST("userlogin")
    Call<LoginResponse> login(@Field("email") String email, @Field("password") String password);

    @FormUrlEncoded
    @POST("createuser")
    Call<ResponseBody> regsiter(@Field("email") String email, @Field("password") String password, @Field("firstName") String firstName, @Field("middleName") String middleName, @Field("lastName") String lastName, @Field("country") String country, @Field("city") String city, @Field("phoneNumber") String phoneNumber, @Field("birthday") String birthday, @Field("gender") String gender);

    @GET("allfeeds")
    Call<FeedsResponse> getFeeds();

    @FormUrlEncoded
    @POST("getactiveUsers")
    Call<ActiveUsersResponse> getActiveUser(@Field("emr_id") String emr_id);

    @FormUrlEncoded
    @POST("getemerengcySpecialInfo")
    Call<EmergencyResponse> getEmergencyInfo(@Field("emr_Id") String emr_id);


    @GET("index.php/AllPosts")
    Call<ListReport> POSTS_CALL();

    @FormUrlEncoded
    @POST("emerengcyPost")
    Call<RegsiterResponse> REPORT_CALL(@Field("longtitude") String longtitude, @Field("latitude") String latitude, @Field("special_Info") String user_Name, @Field("status") String status, @Field("child_id") String child_id, @Field("emr_Date") String emr_Date, @Field("user_Id") String userid);

    @FormUrlEncoded
    @POST("allchildren")
    Call<ChildernResponse> getchildern(@Field("par_Id") String id);

    @FormUrlEncoded
    @POST("addchild")
    Call<RegsiterResponse> addchild(@Field("par_Id") String id, @Field("child_Name") String child_Name, @Field("birthday") String birthday, @Field("gender") String gender2, @Field("child_img_url") String child_img_url, @Field("child_sec_img") String child_sec_img);

    @FormUrlEncoded
    @POST("deleteactiveusers")
    Call<ResponseBody> deleteActiveUser(@Field("user_Id") String user_Id,
                                        @Field("emr_Id") String emr_Id);

    @FormUrlEncoded
    @POST("addActiveUser")
    Call<ResponseBody> addActiveUser(@Field("user_longtitude") String user_longtitude,
                                     @Field("user_latitude") String user_latitude,
                                     @Field("user_Id") String user_Id,
                                     @Field("emr_Id") String emr_Id);

    @FormUrlEncoded
    @POST("childLocated")
    Call<LocationResponse> reportChildLocation(@Field("emr_Id") String emr_id, @Field("par_Id") String par_id);



}
