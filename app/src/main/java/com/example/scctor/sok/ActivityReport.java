package com.example.scctor.sok;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.scctor.model.RegsiterResponse;
import com.example.scctor.network.ClientApiServies;
import com.example.scctor.network.ServiceGenerator;

import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityReport extends AppCompatActivity {

    Intent data;
    ImageView image,imageViews;
    TextView name, gender, age, idpar, idch;
    protected LocationManager locationManager;
    protected LocationListener listener;
    private String lLatitude = "", longitude = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        gps();
        data = getIntent();
        name = findViewById(R.id.name);
        image = findViewById(R.id.images);
        imageViews=findViewById(R.id.profile_image);
        age = findViewById(R.id.age);
        gender = findViewById(R.id.gender);
        name.setText("name:" + data.getStringExtra("name"));
        age.setText("age:" + data.getStringExtra("age"));
        gender.setText("gender:" + data.getStringExtra("gender"));
        Glide.with(this)
                .load(data.getStringExtra("iamge"))
                .into(image);
        Glide.with(this)
                .load(data.getStringExtra("imageViews"))
                .into(imageViews);
        Button bnt = findViewById(R.id.report);
        bnt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClientApiServies serviceGenerator = ServiceGenerator.getApiClient().create(ClientApiServies.class);
                SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                Date date = new Date();

                Call<RegsiterResponse> call = serviceGenerator.REPORT_CALL(longitude, lLatitude, name.getText().toString(), "activei", data.getStringExtra("idch"), formatter.format(date), data.getStringExtra("idpat"));
                call.enqueue(new Callback<RegsiterResponse>() {
                    @Override
                    public void onResponse(Call<RegsiterResponse> call, Response<RegsiterResponse> response) {
                        response.code();
                    }

                    @Override
                    public void onFailure(Call<RegsiterResponse> call, Throwable throwable) {
                        throwable.getMessage();
                    }
                });
            }
        });

    }

    private void gps() {
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, (LocationListener) this);

        listener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {

                lLatitude = String.valueOf(location.getLatitude());
                longitude = String.valueOf(location.getLongitude());


            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }

        };
    }

    ;

}
