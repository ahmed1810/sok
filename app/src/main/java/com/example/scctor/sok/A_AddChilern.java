package com.example.scctor.sok;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.scctor.model.RegsiterResponse;
import com.example.scctor.network.ClientApiServies;
import com.example.scctor.network.ServiceGenerator;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class A_AddChilern extends AppCompatActivity {
    String date;
    private TextView mDisplayDate;
    private DatePickerDialog.OnDateSetListener mDateSetListener;
    ImageView profile_image;
    Spinner spinner0;
    Button addrequest;
    Bitmap bitmap;
    int requestss = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a__add_chilern);
        profile_image = findViewById(R.id.profile_image);
        addrequest = findViewById(R.id.addrequest);
        addrequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                upload();

            }
        });
        profile_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, requestss);

            }
        });

        List<String> gender = new ArrayList<>();
        gender.add("Gender");
        gender.add("Male");
        gender.add("Femal");
        spinner0 = (Spinner) findViewById(R.id.gender);
        ArrayAdapter<String> genderAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, gender);
        genderAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner0.setAdapter(genderAdapter);

        mDisplayDate = (TextView) findViewById(R.id.Date);
        mDisplayDate.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        A_AddChilern.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener,
                        year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;

                date = month + "/" + day + "/" + year;
                mDisplayDate.setText(date);
            }
        };


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == requestss && resultCode == RESULT_OK && data != null) {

            Uri uri = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                profile_image.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    private void upload() {


        ClientApiServies serviceGenerator = ServiceGenerator.getApiClient().create(ClientApiServies.class);
        Call<RegsiterResponse> call = serviceGenerator.addchild("1", "fjdsnfk", "14/2/2019", "male", getEncoded64ImageStringFromBitmap(), getEncoded64ImageStringFromBitmap());
        call.enqueue(new Callback<RegsiterResponse>() {
            @Override
            public void onResponse(Call<RegsiterResponse> call, Response<RegsiterResponse> response) {
                response.code();
            }
            @Override
            public void onFailure(Call<RegsiterResponse> call, Throwable t) {
                t.getMessage();
            }
        });


    }


    public String getEncoded64ImageStringFromBitmap() {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] byteFormat = stream.toByteArray();
        // get the base 64 string
        return Base64.encodeToString(byteFormat, Base64.DEFAULT);

    }

}
