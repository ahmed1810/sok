package com.example.scctor.sok;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.hbb20.CountryCodePicker;

public class register1 extends AppCompatActivity {
    CountryCodePicker countryCodePicker;
    public static String city;
    public static  EditText fname,lname,mname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register1);
        countryCodePicker = findViewById(R.id.ccp);
        fname=findViewById(R.id.firstname);
        lname=findViewById(R.id.lastname);
        mname=findViewById(R.id.middlename);
        countryCodePicker.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                city = countryCodePicker.getSelectedCountryName();
            }
        });
        ImageView img = (ImageView) findViewById(R.id.next);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               if (fname.getText().toString().equals("")||
                       lname.getText().toString().equals("")||mname.getText().toString().equals("")){
                   Toast.makeText(getApplicationContext(),"All fields must be filled in",Toast.LENGTH_LONG).show();
               }
               else {
                   Intent intent = new Intent(register1.this, Register2.class);
                   register1.this.startActivity(intent);
               }
            }


        });

    }
}
