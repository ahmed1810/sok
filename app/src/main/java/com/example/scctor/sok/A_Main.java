package com.example.scctor.sok;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

public class A_Main extends AppCompatActivity {

    private FrameLayout layout;
    TextView screenName;
    DrawerLayout drawerLayout;
    NavigationView navigationView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a__main);

        intialization();
    }

    private void intialization() {
        layout = findViewById(R.id.main_fragment);
        screenName = findViewById(R.id.screen_name);
        drawerLayout = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.navigation);

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                drawerLayout.closeDrawers();
                selectedItemDrawer(menuItem);
                return true;

            }
        });
        startFragment(new AllPost(), "FEEDS");
    }


    public void selectedItemDrawer(MenuItem item){
        switch (item.getItemId()){
            case R.id.feed:
                startFragment(new F_Feeds(), "FEEDS");
                break;
            case R.id.children:
                startFragment(new F_Childern(), "children");
                break;
            case R.id.logout:
                Intent intent = new Intent(A_Main.this, login.class);
                A_Main.this.finish();
                startActivity(intent);
                break;
            case R.id.Report:
                startFragment(new AllPost(), "Report");
                break;
            default:
                return;
        }
        item.setChecked(true);

    }


    private void startFragment(final Fragment fragment, String title) {
//        slidingRootNav.closeMenu(true);

        screenName.setText(title);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.main_fragment, fragment).commit();
//        layout.setVisibility(View.VISIBLE);



//        layout.setVisibility(View.GONE);
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                getSupportFragmentManager().beginTransaction()
//                        .replace(R.id.main_fragment, fragment).commit();
//                layout.setVisibility(View.VISIBLE);
//            }
//        }, 400);


    }

    public void drawerLayout(View view) {
            if (drawerLayout.isDrawerOpen(Gravity.START)) {
                drawerLayout.closeDrawer(Gravity.START);

            } else {
                drawerLayout.openDrawer(Gravity.START);
            }
        }

}
