package com.example.scctor.sok;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.scctor.model.Posts;
import com.example.scctor.network.ClientApiServies;
import com.example.scctor.network.ServiceGenerator;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostAdpter extends RecyclerView.Adapter<PostAdpter.Postview> {

    List<Posts> postsList;
    Context context;

    public PostAdpter(List<Posts> postsList) {
        this.postsList = postsList;
    }

    @NonNull
    @Override
    public PostAdpter.Postview onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_post, viewGroup, false);
        context = viewGroup.getContext();
        return new PostAdpter.Postview(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PostAdpter.Postview postview, int i) {
        final Posts data = postsList.get(i);

        postview.latitude.setText("latitude: " + data.getLatitude());
        postview.longtitude.setText("longtitude: " + data.getLongtitude());
        postview.active_People.setText("active_People: " + data.getActive_People());
        postview.status.setText("status: " + data.getStatus());
        postview.user_Name.setText("user_Name: " + data.getUser_Name());
        postview.feed_Date.setText("feed_Date: " + data.getFeed_Date());
        postview.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, A_ChildMap.class);
                intent.putExtra("post_info", data);
                intent.putExtra("emr_id", data.getEmr_Id());
                addActiveUser(data.getEmr_Id());
                context.startActivity(intent);
            }
        });
    }



    private void addActiveUser(String emr_id) {
        ClientApiServies serviceGenerator = ServiceGenerator.getApiClient().create(ClientApiServies.class);

        Call<ResponseBody> call = serviceGenerator.addActiveUser(AllPost.longitude, AllPost.latitude, "2", emr_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.v("error", t.getMessage());
            }
        });
    }


    @Override
    public int getItemCount() {
        return postsList.size();
    }

    public class Postview extends RecyclerView.ViewHolder {
        TextView emr_Id, longtitude, latitude, feed_Date, user_Name, status, active_People, child_id;

        public Postview(@NonNull View itemView) {
            super(itemView);
            longtitude = itemView.findViewById(R.id.longtitude);
            latitude = itemView.findViewById(R.id.latitude);
            feed_Date = itemView.findViewById(R.id.feed_Date);
            user_Name = itemView.findViewById(R.id.user_Name);
            status = itemView.findViewById(R.id.status);
            active_People = itemView.findViewById(R.id.active_People);

        }
    }
}
