package com.example.scctor.sok;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.scctor.model.allchildern.childern;

import java.util.List;

public class myKindesAdpter extends RecyclerView.Adapter<myKindesAdpter.Kindes> {
    Context context;
    List<childern> list;

    @NonNull
    @Override
    public myKindesAdpter.Kindes onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.kitmes, viewGroup, false);
        context = viewGroup.getContext();
        return new myKindesAdpter.Kindes(view);
    }

    public myKindesAdpter(List<childern> childerns) {
        this.list = childerns;
    }

    @Override
    public void onBindViewHolder(@NonNull myKindesAdpter.Kindes kindes, int i) {
        final childern data = list.get(i);
        kindes.name.setText(data.getChild_Name());
        kindes.age.setText(data.getBirthday());
        kindes.grander.setText(data.getGender());
        Glide.with(context)
                .load(data.getChild_img_url())
                .into(kindes.imageView);

        kindes.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ActivityReport.class);
                intent.putExtra("name", data.getChild_Name());
                intent.putExtra("age", data.getBirthday());
                intent.putExtra("grander", data.getGender());
                intent.putExtra("iamge", data.getChild_img_url());
                intent.putExtra("idpat", data.getPar_Id());
                intent.putExtra("idch", data.getCh_Id());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class Kindes extends RecyclerView.ViewHolder {
        TextView name, age, grander;
        ImageView imageView;

        public Kindes(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.idname);
            age = itemView.findViewById(R.id.age);
            grander = itemView.findViewById(R.id.grander);
            imageView = itemView.findViewById(R.id.profile_image);

        }
    }
}
