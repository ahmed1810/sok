package com.example.scctor.sok;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.scctor.model.allchildern.ChildernResponse;
import com.example.scctor.network.ClientApiServies;
import com.example.scctor.network.ServiceGenerator;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class F_Childern extends Fragment {

    private View view;
    RecyclerView recyclerView;
    RecyclerView.LayoutManager Manager;
    myKindesAdpter myKindesAdpter;

    public F_Childern() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_f__childern, container, false);
        initialization();
        getChildern();
        return view;
    }

    private void initialization() {
        recyclerView = view.findViewById(R.id.childern);
        Manager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(Manager);
    }

    private void getChildern() {

        ClientApiServies serviceGenerator = ServiceGenerator.getApiClient().create(ClientApiServies.class);

        Call<ChildernResponse> call = serviceGenerator.getchildern("1");
        call.enqueue(new Callback<ChildernResponse>() {
            @Override
            public void onResponse(Call<ChildernResponse> call, Response<ChildernResponse> response) {
                if (response.body().getError().equals("false")) {
                    response.code();
                    myKindesAdpter = new myKindesAdpter(response.body().getChilderns());
                    recyclerView.setAdapter(myKindesAdpter);

                }

            }

            @Override
            public void onFailure(Call<ChildernResponse> call, Throwable t) {
                t.getMessage();

            }
        });

    }

}
