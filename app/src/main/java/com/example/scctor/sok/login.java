package com.example.scctor.sok;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.scctor.model.LoginRequest;
import com.example.scctor.model.LoginResponse;
import com.example.scctor.network.ClientApiServies;
import com.example.scctor.network.ServiceGenerator;
import com.example.scctor.utilities.SharedPrefsHelper;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class login extends AppCompatActivity {
    AlertDialog.Builder builder;


    EditText pass,email;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        email=findViewById(R.id.email);
        pass=findViewById(R.id.pass);

        final Button blog= (Button)findViewById(R.id.login);
        builder=new AlertDialog.Builder(login.this);
        blog.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                try {
                    ClientApiServies serviceGenerator = ServiceGenerator.getApiClient().create(ClientApiServies.class);
                    Call<LoginResponse> call = serviceGenerator.login(email.getText().toString(),pass.getText().toString());
                    call.enqueue(new Callback<LoginResponse>() {
                        @Override
                        public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                            if (response.body().getError().equals("true")){
                                builder.setTitle("Error");
                                blog.setClickable(false);
                                builder.setMessage(response.body().getMessage());
                                builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();

                                    }
                                });
                            }else {

                                SharedPrefsHelper.getInstance(login.this).setUserName(response.body().getUser().getFirstName());

                                builder.setTitle("successful");
                                blog.setClickable(false);
                                builder.setMessage(response.body().getMessage());
                                builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        startActivity(new Intent(login.this, A_Main.class));
                                    }
                                });

                            }
                            builder.show();

                        }

                        @Override
                        public void onFailure(Call<LoginResponse> call, Throwable t) {
                            t.getMessage();

                        }
                    });

                }catch (Exception  e){
e.getMessage();
                }

            }
//                Intent intent = new Intent(login.this, MainActivity.class);
//                login.this.startActivity(intent);            }
        });


    }
}
