package com.example.scctor.sok;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.scctor.model.feeds.Feeds;

import java.util.List;

import at.blogc.android.views.ExpandableTextView;

public class FeedsAdapter extends RecyclerView.Adapter<FeedsAdapter.FeedViewHolder> {

    Context context;
    List<Feeds> feeds;

    public FeedsAdapter(List<Feeds> feeds) {
        this.feeds = feeds;
    }

    @NonNull
    @Override
    public FeedViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.feeds_item,
                viewGroup, false);
        context = viewGroup.getContext();
        return new FeedViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull FeedViewHolder feedViewHolder, int i) {
        feedViewHolder.date.setText(feeds.get(i).getFeed_Date());
        feedViewHolder.username.setText(feeds.get(i).getUser_Name());
        feedViewHolder.feedContent.setText(feeds.get(i).getContent());
        Glide.with(context)
                .load(feeds.get(i).getFeed_img())
                .into(feedViewHolder.feedImage);

        extandableTextView(feedViewHolder.feedContent);

    }

    private void extandableTextView(final ExpandableTextView feedContent) {
        feedContent.setInterpolator(new OvershootInterpolator());

// or set them separately
        feedContent.setExpandInterpolator(new OvershootInterpolator());
        feedContent.setCollapseInterpolator(new OvershootInterpolator());

        feedContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (feedContent.isExpanded()) {
                    feedContent.collapse();
//                    buttonToggle.setText(R.string.expand);
                } else {
                    feedContent.expand();
//                    buttonToggle.setText(R.string.collapse);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return feeds.size();
    }

    class FeedViewHolder extends RecyclerView.ViewHolder {

        TextView username, date;
        ImageView feedImage;
        ExpandableTextView feedContent;

        public FeedViewHolder(@NonNull View itemView) {
            super(itemView);
            username = itemView.findViewById(R.id.username);
            feedContent = itemView.findViewById(R.id.feed_content);
            date = itemView.findViewById(R.id.date);
            feedImage = itemView.findViewById(R.id.feed_image);
        }
    }

}
