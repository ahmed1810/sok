package com.example.scctor.sok;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.scctor.model.LoginResponse;
import com.example.scctor.model.RegsiterResponse;
import com.example.scctor.network.ClientApiServies;
import com.example.scctor.network.ServiceGenerator;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Register2 extends AppCompatActivity {
    private static final String TAG = "Register2";
    private TextView mDisplayDate;
    private DatePickerDialog.OnDateSetListener mDateSetListener;
    private EditText email, pass, phone;
    String date;
    Spinner spinner0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register2);
        List<String> gender = new ArrayList<>();
        gender.add("Gender");
        gender.add("Male");
        gender.add("Femal");
        email = findViewById(R.id.email);
        pass = findViewById(R.id.pass);
        phone = findViewById(R.id.phone);


        spinner0 = (Spinner) findViewById(R.id.gender);
        ArrayAdapter<String> genderAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, gender);
        genderAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner0.setAdapter(genderAdapter);

        mDisplayDate = (TextView) findViewById(R.id.Date);
        mDisplayDate.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        Register2.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener,
                        year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                Log.d(TAG, "onDateSet: mm/dd/yyy: " + month + "/" + day + "/" + year);

                date = month + "/" + day + "/" + year;
                mDisplayDate.setText(date);
            }
        };


    }

    public void regsiter(View view) {

        if (pass.getText().toString().equals("")||email.getText().toString().equals("")||
                phone.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(),"All fields must be filled in",Toast.LENGTH_LONG).show();

        }else {
            try {

                ClientApiServies serviceGenerator = ServiceGenerator.getApiClient().create(ClientApiServies.class);
                Call<ResponseBody> call = serviceGenerator.regsiter(email.getText().toString(), pass.getText().toString(), register1.fname.getText().toString(), register1.mname.getText().toString(), register1.lname.getText().toString(), register1.city, register1.city, phone.getText().toString(), date, spinner0.getSelectedItem().toString());
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.code() == 201) {
                            if (response.body().source().buffer().toString().contains("User created successfully")) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(Register2.this);
                                builder.setMessage("User created successfully");
                                builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        startActivity(new Intent(Register2.this, A_Main.class));
                                    }
                                });
                                builder.show();
                            }

                        } else {
                            Toast.makeText(getApplicationContext(), "Not User created ", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        t.getMessage();

                    }
                });

            } catch (Exception e) {
                e.getMessage();
            }
        }

    }

}

