package com.example.scctor.sok;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.scctor.child_located.LocationResponse;
import com.example.scctor.model.Posts;
import com.example.scctor.model.active_users.ActiveUsersResponse;
import com.example.scctor.model.active_users.Active_Users;
import com.example.scctor.model.emergencyInfo.EmergencyResponse;
import com.example.scctor.network.ClientApiServies;
import com.example.scctor.network.ServiceGenerator;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class A_ChildMap extends FragmentActivity implements OnMapReadyCallback, LocationListener {

    private GoogleMap mMap;
    private List<Active_Users> activeUsers;
    private LatLng latLng;
    private TextView gender, name, age, time, state, active, specialInfo, childSaved;
    Intent data;
    private String emrId;
    private CircleImageView childPhoto;
    private Button childLocated, contactParent;
    protected LocationManager locationManager;
    protected LocationListener locationListener;
    protected String latitude, longitude, phone;
    private LinearLayout infoLayout;
    private ProgressDialog dialog;
    private Posts postInfo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_child_map);

        intializtion();
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        getLocation();
        getEmergencyInfo();
        activeUsers();
    }

    private void setChildLocation() {

        latLng = new LatLng(Double.parseDouble(postInfo.getLatitude()),
                Double.parseDouble(postInfo.getLongtitude()));

        MarkerOptions marker = new MarkerOptions().position(latLng).
                title("Child");

        marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.marker2));
        mMap.addMarker(marker);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10.0f));


    }

    private void getLocation() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 5000, this);
    }

    private void intializtion() {
        data = getIntent();
        emrId = data.getStringExtra("emr_id");
        postInfo = data.getExtras().getParcelable("post_info");
        gender = findViewById(R.id.gender);
        name = findViewById(R.id.username);
        age = findViewById(R.id.age);
        state = findViewById(R.id.state);
        time = findViewById(R.id.time);
        active = findViewById(R.id.active);
        childPhoto = findViewById(R.id.child_image);
        specialInfo = findViewById(R.id.info);
        childLocated = findViewById(R.id.child_located);
        contactParent = findViewById(R.id.contact_volunteer);
        infoLayout = findViewById(R.id.info_layout);
        childSaved = findViewById(R.id.child_Saved);
        dialog = new ProgressDialog(this);
        dialog.setMessage("Please Waite ...");
        dialog.setCancelable(false);
        dialog.show();
        childLocated.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reportChildLocation();
            }
        });

        contactParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_CALL);

                intent.setData(Uri.parse("tel:" + "011"));
                startActivity(intent);
            }
        });

    }

    @Override
    protected void onStop() {
        super.onStop();
        deleteActiveUser();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney, Australia, and move the camera.
//        LatLng sydney = new LatLng(-34, 151);
//        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }

    private void activeUsers() {
        ClientApiServies serviceGenerator = ServiceGenerator.getApiClient().create(ClientApiServies.class);

        Call<ActiveUsersResponse> call = serviceGenerator.getActiveUser(emrId);
        call.enqueue(new Callback<ActiveUsersResponse>() {
            @Override
            public void onResponse(Call<ActiveUsersResponse> call, Response<ActiveUsersResponse> response) {
                setChildLocation();

                activeUsers = response.body().getActive_Users();

                for (int i = 0; i < activeUsers.size(); i++) {
                    latLng = new LatLng(Double.parseDouble(activeUsers.get(i).getUser_latitude()),
                            Double.parseDouble(activeUsers.get(i).getUser_longtitude()));

                    MarkerOptions marker = new MarkerOptions().position(latLng).
                            title(activeUsers.get(i).getActive_Id());

                    marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.marker1));
                    mMap.addMarker(marker);


                }

                dialog.dismiss();
            }

            @Override
            public void onFailure(Call<ActiveUsersResponse> call, Throwable t) {
                Log.v("error", t.getMessage());
                dialog.dismiss();
            }
        });
    }


    private void getEmergencyInfo() {
        ClientApiServies serviceGenerator = ServiceGenerator.getApiClient().create(ClientApiServies.class);

        Call<EmergencyResponse> call = serviceGenerator.getEmergencyInfo(emrId);
        call.enqueue(new Callback<EmergencyResponse>() {
            @Override
            public void onResponse(Call<EmergencyResponse> call, Response<EmergencyResponse> response) {
                gender.setText(response.body().getEmergency_Info().get(0).getGender());
                name.setText(response.body().getEmergency_Info().get(0).getChild_Name());
                time.setText(response.body().getEmergency_Info().get(0).getEmr_Date().toString());
                String activeUsers = response.body().getEmergency_Info().get(0).getActive_People();
                activeUsers = activeUsers.replace('-', ' ');
                active.setText("Active: "+ activeUsers);
                age.setText(response.body().getEmergency_Info().get(0).getBirthday());
                state.setText(response.body().getEmergency_Info().get(0).getStatus());
                specialInfo.setText(response.body().getEmergency_Info().get(0).getSpecial_Info());
                Glide.with(A_ChildMap.this)
                        .load(response.body().getEmergency_Info().get(0).getChild_img_url())
                        .into(childPhoto);
            }

            @Override
            public void onFailure(Call<EmergencyResponse> call, Throwable t) {
                Log.v("error", t.getMessage());
            }
        });
    }


    private void reportChildLocation() {
        ClientApiServies serviceGenerator = ServiceGenerator.getApiClient().create(ClientApiServies.class);

        Call<LocationResponse> call = serviceGenerator.reportChildLocation(emrId, "1");
        call.enqueue(new Callback<LocationResponse>() {
            @Override
            public void onResponse(Call<LocationResponse> call, Response<LocationResponse> response) {
                phone = response.body().getParent_Info().get(0).getPhoneNumber();
                childLocated.setVisibility(View.GONE);
                contactParent.setVisibility(View.VISIBLE);
                infoLayout.setVisibility(View.GONE);
                childSaved.setVisibility(View.VISIBLE);
                mMap.clear();

            }

            @Override
            public void onFailure(Call<LocationResponse> call, Throwable t) {
                Log.v("error", t.getMessage());
            }
        });
    }


    private void deleteActiveUser() {
        ClientApiServies serviceGenerator = ServiceGenerator.getApiClient().create(ClientApiServies.class);

        Call<ResponseBody> call = serviceGenerator.deleteActiveUser("1", emrId);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.v("respnse", response.body().toString());
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.v("error", t.getMessage());
            }
        });
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onLocationChanged(Location location) {
        latitude = String.valueOf(location.getLatitude());
        longitude = String.valueOf(location.getLongitude());
        latLng = new LatLng(Double.parseDouble(latitude),
                Double.parseDouble(longitude));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
//        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10.0f));

        //     addActiveUser();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }
}