package com.example.scctor.sok;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.scctor.model.LoginResponse;
import com.example.scctor.model.feeds.FeedsResponse;
import com.example.scctor.network.ClientApiServies;
import com.example.scctor.network.ServiceGenerator;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */

public class F_Feeds extends Fragment {

    RecyclerView feedsRecycler;
    RecyclerView.LayoutManager feedsManager;
    FeedsAdapter feedsAdapter;
    View view;


    public F_Feeds() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_f__feeds, container, false);
        initialization();
        getFeeds();
        return view;
    }

    private void initialization() {
        feedsRecycler = view.findViewById(R.id.feeds_recycler);
        feedsManager = new LinearLayoutManager(getActivity());
        feedsRecycler.setLayoutManager(feedsManager);
    }

    private void getFeeds(){
        ClientApiServies serviceGenerator = ServiceGenerator.getApiClient().create(ClientApiServies.class);

        Call<FeedsResponse> call = serviceGenerator.getFeeds();
        call.enqueue(new Callback<FeedsResponse>() {
            @Override
            public void onResponse(Call<FeedsResponse> call, Response<FeedsResponse> response) {
                feedsAdapter = new FeedsAdapter(response.body().getFeeds());
                feedsRecycler.setAdapter(feedsAdapter);
            }

            @Override
            public void onFailure(Call<FeedsResponse> call, Throwable t) {
                Log.v("a7a", t.toString());
            }
        });
    }

}
