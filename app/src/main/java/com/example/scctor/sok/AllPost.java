package com.example.scctor.sok;


import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.scctor.model.ListReport;
import com.example.scctor.network.ClientApiServies;
import com.example.scctor.network.ServiceGenerator;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;



/**
 * A simple {@link Fragment} subclass.
 */
public class AllPost extends Fragment  implements LocationListener{

    private View view;
    RecyclerView recyclerView;
    RecyclerView.LayoutManager Manager;
    PostAdpter postAdpter;
    protected LocationManager locationManager;
    protected LocationListener locationListener;
    public static String latitude, longitude;


    public AllPost() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_all_post, container, false);

        getLocation();
        initialization();
        getReport();
        return view;
    }

    private void getReport() {

        ClientApiServies serviceGenerator = ServiceGenerator.getApiClient().create(ClientApiServies.class);

        Call<ListReport> call = serviceGenerator.POSTS_CALL();
        call.enqueue(new Callback<ListReport>() {
            @Override
            public void onResponse(Call<ListReport> call, Response<ListReport> response) {
                if (response.body().getError().equals("false")) {
                    response.code();
                    postAdpter = new PostAdpter(response.body().getPosts());
                    recyclerView.setAdapter(postAdpter);

                }
            }

            @Override
            public void onFailure(Call<ListReport> call, Throwable throwable) {
                Log.d("error", throwable.getMessage());
            }
        });

    }

    private void initialization() {
        recyclerView = view.findViewById(R.id.recycalviews);
        Manager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(Manager);

    }

    private void getLocation() {
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 5000, this);
    }


    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onLocationChanged(Location location) {
        latitude = String.valueOf(location.getLatitude());
        longitude = String.valueOf(location.getLongitude());
        //     addActiveUser();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }


}
