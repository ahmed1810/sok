package com.example.scctor.utilities;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Locale;

public class SharedPrefsHelper {

    private final String MY_PREFS = "MY_PREFS";
    private final String UserName = "UserName";
    private SharedPreferences sharedPreferences;
    private static SharedPrefsHelper prefsHelper;

    private SharedPrefsHelper(Context context) {
        sharedPreferences = context.getSharedPreferences(MY_PREFS, Context.MODE_PRIVATE);
    }

    public static SharedPrefsHelper getInstance(Context context) {
        if (prefsHelper == null) {
            prefsHelper = new SharedPrefsHelper(context);
        }
        return prefsHelper;
    }


    public String getUserName() {
        return sharedPreferences.getString(UserName, null);
    }

    public void setUserName(String name) {
        sharedPreferences.edit().putString(UserName, name).apply();

    }
}
